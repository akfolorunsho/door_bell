import 'dart:io';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:path_provider/path_provider.dart';


final _auth = FirebaseAuth.instance;
final _firestore = Firestore.instance;
var _uri;

class SettingsTab extends StatefulWidget {
  static String id;

  @override
  _SettingsTab createState() => _SettingsTab();
}

class _SettingsTab extends State<SettingsTab> {
  TextEditingController _nameController;
  TextEditingController _emailController;
  TextEditingController relativeNumberController;
  String _nameLabel;
  String _relativeNumberLabel = 'Enter relative Number';

  String _email;
  File _image;

  FocusNode _nameFocus;
  FocusNode _relativeNumberFocus;

  _listenForChanges() async {
    FirebaseUser _user = await _auth.currentUser();
    String phoneNumber = _user.displayName.split(" ").last;
    await _firestore
        .collection('users')
        .document(phoneNumber)
        .snapshots()
        .listen(
          (data) {
        if (this.mounted) {
          setState(
                () {
              _nameLabel = data.data["name"];
              _relativeNumberLabel = data.data["Relative Number"];
            },
          );
        }
      },
    );
  }

  Widget _buildNameTextField() {
    return TextField(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) => CustomDialog(
            title: "Enter Your Name",
            whatToChange: "name",
          ),
        );
        FocusScope.of(context).requestFocus(FocusNode());
      },
      textCapitalization: TextCapitalization.words,
      focusNode: _nameFocus,
      controller: _nameController,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hasFloatingPlaceholder: false,
        prefixIcon: Icon(
          Icons.person,
          color: Colors.black45,
          size: 25,
        ),
        suffixIcon: Icon(
          Icons.edit,
          color: Colors.black45,
          size: 25,
        ),
        labelText: _nameLabel,
      ),
    );
  }

  Widget _buildEmailTextField() {
    return TextField(
      enabled: false,
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        prefixIcon: Icon(
          Icons.person,
          color: Colors.black45,
          size: 25,
        ),
        labelText: _email,
      ),
    );
  }

  Widget _buildRelativeNumberTextField() {
    return TextField(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) => CustomDialog(
            title: "Enter Relative Number",
            whatToChange: "Relative Number",
          ),
        );
        FocusScope.of(context).requestFocus(FocusNode());
      },
      focusNode: _relativeNumberFocus,
      controller: relativeNumberController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        hasFloatingPlaceholder: false,
        prefixIcon: Icon(
          Icons.phone_android,
          color: Colors.black45,
          size: 25,
        ),
        suffixIcon: Icon(
          Icons.edit,
          color: Colors.black45,
          size: 25,
        ),
        labelText: _relativeNumberLabel,
      ),
    );
  }

  Widget _userLogOut() {
    return GestureDetector(
      child: Row(
        children: <Widget>[
          Icon(
            Icons.lock_open,
            color: Colors.black45,
          ),
          SizedBox(width: 5),
          Text(
            'Logout',
            style: TextStyle(color: Colors.black45, fontSize: 15),
          ),
        ],
      ),
      onTap: () async {
        _auth.signOut();
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        _preferences.setString("logIn", "login");
        _preferences.setBool('firstLaunch', true);
        Navigator.pushReplacementNamed(context, "/login");
      },
    );
  }

  Future _getImage() async {
    FirebaseUser _user = await _auth.currentUser();
    String number = _user.displayName.split(" ").last;
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    File croppedImage = await ImageCropper.cropImage(
        sourcePath: image.path,
        maxHeight: 350,
        maxWidth: 350,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Theme.of(context).primaryColor,
            toolbarWidgetColor: Colors.white,
            activeControlsWidgetColor: Theme.of(context).primaryColor,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: true),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));

    var appDirectory = await getTemporaryDirectory();

    File imageCompressed = await FlutterImageCompress.compressAndGetFile(
        croppedImage.path, appDirectory.path + '/' + number + '.jpg',
        quality: 50);

    File imageCompressedThumbnail =
    await FlutterImageCompress.compressAndGetFile(imageCompressed.path,
        appDirectory.path + '/' + number + 'th' + '.jpg',
        minWidth: 70, minHeight: 70, quality: 20);

    setState(() {
      _image = croppedImage;
    });

    final StorageReference storageRef =
    FirebaseStorage.instance.ref().child(number);

    final StorageReference storageRefTh =
    FirebaseStorage.instance.ref().child('thumbnails/$number' + 'th');

    storageRef.putFile(imageCompressed);
    storageRefTh.putFile(imageCompressedThumbnail);
    String _thumbnail = (await storageRefTh.getDownloadURL()).toString();

    UserUpdateInfo userUpdateInfo = UserUpdateInfo();
    userUpdateInfo.photoUrl = _thumbnail;
    _user.updateProfile(userUpdateInfo);

    QuerySnapshot snapshots = await _firestore
        .collection('users')
        .where('phone', isEqualTo: number)
        .getDocuments();

    for (var snaps in snapshots.documents) {
      if (snaps.data['urlThumbnail'] != _thumbnail) {
        await _firestore.collection('users').document(number).updateData({
          'urlThumbnail': _thumbnail,
        });
      }
    }
    QuerySnapshot users = await _firestore
        .collection('users')
        .getDocuments(); //get all users on doorbell

    for (var contacts in users.documents) {
      QuerySnapshot doc = await _firestore
          .document('users/${contacts.documentID}')
          .collection('contacts')
          .where('number', isEqualTo: number)
          .getDocuments(); //get the document of users that have me as a contact
      for (var user in doc.documents) {
        user.reference.updateData({'urlThumbnail': _thumbnail});
      }
    }
  }

  _getDp() async {
    FirebaseUser _user = await _auth.currentUser();
    String number = _user.displayName.split(" ").last;
    final ref = FirebaseStorage.instance.ref().child(number);
    var url = await ref.getDownloadURL();
    setState(() {
      _uri = url;
    });
    print("uri" + _uri);
  }

  void _listenForChangeInRinger() async {
    FirebaseUser _user = await _auth.currentUser();
    String phoneNumber = _user.displayName
        .split(" ")
        .last;
    await _firestore
        .collection('users')
        .document(phoneNumber)
        .collection('ringer')
        .snapshots()
        .listen(
          (data) {
        if (this.mounted) {
          if(data.documents.isNotEmpty){
            Navigator.pushNamed(context, "/byTheDoor");
          }
        }
      },
    );
  }


  @override
  void initState() {
    _listenForChangeInRinger();
    _getUser();
    _nameFocus = FocusNode();
    _relativeNumberFocus = FocusNode();
    _listenForChanges();
    _getDp();
    super.initState();
  }

  _getUser() async {
    FirebaseUser _user = await _auth.currentUser();
    print(_email);
    String number = _user.displayName.split(" ").last;
    DocumentSnapshot snap =
    await _firestore.collection("users").document(number).get();
    setState(() {
      _email = _user.email;
      _nameLabel = _user.displayName.split(" ").first +
          " " +
          _user.displayName.split(" ")[1];
      _relativeNumberLabel = (snap.data["Relative Number"] == null)
          ? "Relative Number"
          : snap.data["Relative Number"];
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          title: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Settings',
              style: TextStyle(color: Color(0X0FF212121),
                  fontSize: MediaQuery.of(context).size.width * 0.045),
            ),
          ),
          backgroundColor: Theme.of(context).primaryColor,
          iconTheme: IconThemeData(color: Theme.of(context).primaryColorLight),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.06,),
            alignment: Alignment.center,
            child: Column(
              children: <Widget>[
                Container(
                  child: GestureDetector(
                    onTap: () => _getImage(),
                    child: ClipOval(
                      child: (_image == null)
                          ? (_uri != null)
                          ? Image(
                          fit: BoxFit.cover,
                          width:
                          MediaQuery.of(context).size.width * 0.5,
                          height:
                          MediaQuery.of(context).size.width * 0.5,
                          image: NetworkImage(_uri))
                          : CircleAvatar(
                        radius:
                        MediaQuery.of(context).size.width * 0.25,
                        backgroundColor:
                        Theme.of(context).primaryColorLight,
                        child: Icon(
                          Icons.person,
                          color: Color(0XFFBDBDBD),
                          size: MediaQuery.of(context).size.width *
                              0.38,
                        ),
                      )
                          : Image.file(
                        _image,
                        fit: BoxFit.cover,
                        width: MediaQuery.of(context).size.width * 0.5,
                        height: MediaQuery.of(context).size.width * 0.5,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.015,),
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Column(
                    children: <Widget>[
                      _buildNameTextField(),
                      _buildRelativeNumberTextField(),
                      _buildEmailTextField(),
                      Container(
                        alignment: Alignment.topLeft,
                        margin: EdgeInsets.only(top: 50),
                        child: _userLogOut(),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CustomDialog extends StatelessWidget {
  final String title, whatToChange;
  final Image image;
  final GlobalKey<FormState> _editNameKey = GlobalKey<FormState>();

  CustomDialog({
    @required this.title,
    @required this.whatToChange,
    this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: 16,
              bottom: 16,
              left: 16,
              right: 16,
            ),
            decoration: new BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10.0,
                  offset: const Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Form(
              key: _editNameKey,
              child: Column(
                mainAxisSize: MainAxisSize.min, // To make the card compact
                children: <Widget>[
                  Text(
                    title,
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 24.0,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(height: 16.0),
                  TextFormField(
                    textCapitalization: TextCapitalization.words,
                    keyboardType: (whatToChange == "name")
                        ? TextInputType.text
                        : TextInputType.number,
                    validator: (whatToChange == "name")
                        ? (String val) {
                      if (val.isEmpty) {
                        return 'Input your name';
                      } else if (val.trim().split(" ").length < 2) {
                        return 'First Name and Last Name must be inputed';
                      } else {
                        return null;
                      }
                    }
                        : (String val) {
                      String pattern = r'(^([0][7-9])[0-9]{9}$)';
                      RegExp regex = new RegExp(pattern);
                      if (val.isEmpty) {
                        return 'Input your phone number';
                      } else if (!regex.hasMatch(val)) {
                        return 'invalid number: should start will \'0\' and contain 11 digits';
                      } else {
                        return null;
                      }
                    },
                    decoration: InputDecoration(
                      hasFloatingPlaceholder: false,
                      labelStyle: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: MediaQuery.of(context).size.width * 0.04),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 3.0,
                              color: Theme.of(context).primaryColor),
                          borderRadius: BorderRadius.circular(15)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context).accentColor, width: 3),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    onSaved: (String value) async {
                      FirebaseUser user = await _auth.currentUser();
                      String number = user.displayName.split(" ").last;
                      await _firestore
                          .collection('users')
                          .document(number)
                          .updateData({whatToChange: value});
                      if (whatToChange == 'name') {
                        QuerySnapshot users =
                        await _firestore.collection('users').getDocuments();
                        for (var contacts in users.documents) {
                          QuerySnapshot doc = await _firestore
                              .document('users/${contacts.documentID}')
                              .collection('contacts')
                              .where('name', isEqualTo: value)
                              .getDocuments();
                          for (var user in doc.documents) {
                            user.reference.updateData({'name': value});
                          }
                        }
                      }
                    },
                  ),
                  SizedBox(height: 24.0),
                  Align(
                      alignment: Alignment.bottomRight,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            FlatButton(
                              onPressed: () async {
                                Navigator.of(context).pop();
                              },
                              child: Text('Cancel'),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            FlatButton(
                              onPressed: () async {
                                if (_editNameKey.currentState.validate()) {
                                  _editNameKey.currentState.save();
                                }
                                Navigator.of(context).pop();
                                // To close the dialog
                              },
                              child: Text(
                                'Save',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                          ])),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
