import 'dart:io';

import 'package:door_bell/byTheDoor.dart';
import 'package:door_bell/homePage.dart';
import 'package:door_bell/settingsTab.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import './signupPage.dart';
import './loginPage.dart';

final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
var globalKey = GlobalKey<NavigatorState>();

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  return showNotification(message);
}

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

showNotification(Map<String, dynamic> message) async {
  var initializationSettingsAndroid =
      new AndroidInitializationSettings('@mipmap/ic_launcher');
  var initializationSettingsIOS = IOSInitializationSettings();
  var initializationSettings = InitializationSettings(
      initializationSettingsAndroid, initializationSettingsIOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'channel', 'DoorBell', 'channel description',
      importance: Importance.Max,
      priority: Priority.High,
      ticker: 'ticker',
      sound: 'sound',
      playSound: true,
      enableLights: true,
      enableVibration: true,
      color: Color(0xFFFF9800),
      icon: '@mipmap/ic_launcher'
  );

  var iOSPlatformChannelSpecifics = IOSNotificationDetails();

  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

  await flutterLocalNotificationsPlugin.show(
      0,
      'DOORBELL',
      message["data"]['senderName'] + " " + 'is by the door',
      platformChannelSpecifics,
      payload: 'item');
}




void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Widget _defaultPage = new SignupPage();
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String logOn = preferences.getString('logIn');
  NotificationAppLaunchDetails det = await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

  if (det.didNotificationLaunchApp) {
    _defaultPage = new ByTheDoor();
  } else {
    if (logOn == "home") {
      _defaultPage = new HomePage();
    } else if (logOn == "login") {
      _defaultPage = new LoginPage();
    } else {
      _defaultPage = new SignupPage();
    }
  }

  if (Platform.isIOS) {
    iOsPermission();
  }
  setPortraitOrientation();

  void onLaunch(Map message) async {
  }

  void onResume(Map message) async {
  }

  void onMessage(Map message) async {
  }

  _firebaseMessaging.configure(
    onLaunch: ((Map<String, dynamic> message) async {
      onLaunch(message);
    }),
    onResume: (Map<String, dynamic> message) async {
      onResume(message);
    },
    onBackgroundMessage: myBackgroundMessageHandler,
    onMessage: (Map<String, dynamic> message) async {
      onMessage(message);
    },
  );

  runApp(
    MaterialApp(
      navigatorKey: globalKey,
      title: 'Doorbell',
      theme: ThemeData(
          primaryColor: Color(0xFFFF9800),
          primaryColorDark: Color(0XFFF57C00),
          primaryColorLight: Color(0XFFFFE0B2),
          accentColor: Color(0xFFFFC107),
          errorColor: Color(0XFFFF52),
          buttonTheme: ButtonThemeData(
            minWidth: 95,
          )),
      home: _defaultPage,
      routes: <String, WidgetBuilder>{
        '/signUp': (context) => SignupPage(),
        '/login': (context) => LoginPage(),
        '/homePage': (context) => HomePage(),
        '/byTheDoor': (context) => ByTheDoor(),
        '/settingsTab': (context) => SettingsTab(),
      },
    ),
  );
}

setPortraitOrientation() async {
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
}

Future onSelectNotification(String payload) async{
  await  globalKey.currentState.push(MaterialPageRoute(builder: (context) => ByTheDoor()));
}

void iOsPermission() {
  _firebaseMessaging.requestNotificationPermissions(
      IosNotificationSettings(sound: true, badge: true, alert: true));
  _firebaseMessaging.onIosSettingsRegistered
      .listen((IosNotificationSettings settings) {
    print("Settings registered: $settings");
  });
}
