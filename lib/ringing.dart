import 'package:audioplayers/audio_cache.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';


class Ringing extends StatefulWidget {
  final String number;
  final String urlThumbnail;

  final String token;

  Ringing(this.number, this.token, this.urlThumbnail);

  @override
  State createState() => _Ringing();
}

class _Ringing extends State<Ringing> {
  final _firestore = Firestore.instance;
  final _auth = FirebaseAuth.instance;
  String reply = 'Bell Ringing';
  AudioCache cache = new AudioCache();
  AudioPlayer player = new AudioPlayer();
  bool affirmBell = true;
  String image = "assets/logo.png";
  Color color = Colors.white;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  final HttpsCallable callable = CloudFunctions.instance.getHttpsCallable(
    functionName: 'sendNotifications',
  );

  void _listenForChangeInRinger() async {
    FirebaseUser _user = await _auth.currentUser();
    String phoneNumber = _user.displayName
        .split(" ")
        .last;
    await _firestore
        .collection('users')
        .document(phoneNumber)
        .collection('ringer')
        .snapshots()
        .listen(
          (data) {
        if (this.mounted) {
          if(data.documents.isNotEmpty){
            Navigator.pushNamed(context, "/byTheDoor");
          }
        }
      },
    );
  }

  @override
  void initState() {
    _listenForChangeInRinger();
    super.initState();
    _listenForReply();
    _getUserInfoAndRing();
  }

  _getUserInfoAndRing() async {
    String myToken = await _firebaseMessaging.getToken();
    FirebaseUser _user = await _auth.currentUser();
    String number = _user.displayName.split(" ").last;
    String name = _user.displayName.split(" ").first +
        " " +
        _user.displayName.split(" ")[1];

    await callable.call(<String, dynamic>{
      'token': widget.token,
      'name': name,
      'number': number,
      'senderToken': myToken,
      'receiverNumber': widget.number
    }).then((value) {
      _affirmBell();
    });
  }

  _listenForReply() async {
    FirebaseUser _user = await _auth.currentUser();
    String phoneNumber = _user.displayName.split(" ").last;
    await _firestore
        .collection('users')
        .document(phoneNumber)
        .snapshots()
        .listen(
          (data) {
        if (this.mounted) {
          setState(
                () {
              reply = data.data["reply"];
            },
          );
        }
      },
    );
  }

  _affirmBell() async {
    const alarmAudioPath = "sound.mp3";
    (affirmBell)? player = await cache.play(alarmAudioPath): player.stop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            affirmBell=false;
            _affirmBell();
            Navigator.pop(context);
          },
        ),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height*.02, top: MediaQuery.of(context).size.height*.2,),
              width: MediaQuery.of(context).size.width * 0.5,
              height: MediaQuery.of(context).size.width * 0.5,
              child: CircleAvatar(
                backgroundImage: (widget.urlThumbnail == null)
                    ? AssetImage(image)
                    : NetworkImage(widget.urlThumbnail),
                backgroundColor: Colors.white,
                // radius: 50,
              ),
            ),
            Text(
              (reply != null) ? reply : "Bell Ringing",
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.06,
                color: color,
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.7,
              height: MediaQuery.of(context).size.height * 0.06,
              margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.1),
              child: RaisedButton(
                elevation: 5,
                color: Colors.white,
                child: Text(
                  'Cancel Bell',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 25),
                ),
                onPressed: () async {
                  affirmBell=false;
                  _affirmBell();

                  Navigator.pop(context);
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  onError(e) {
    print(e.message);
    Fluttertoast.showToast(
        msg: 'Unable to connect',
        toastLength: Toast.LENGTH_LONG,
        backgroundColor: Colors.transparent,
        textColor: Colors.black);
  }
}
