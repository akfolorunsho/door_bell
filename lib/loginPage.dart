import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'forgotPassword.dart';

class LoginPage extends StatefulWidget {
  static String id;

  @override
  _LoginPage createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {
  final GlobalKey<FormState> _formKey2 = GlobalKey<FormState>();
  String _username;
  String _password;
  final _auth = FirebaseAuth.instance;
  bool _isLoading = false;
  bool _isError = false;
  String _errorText;
  FirebaseUser _newUser;
  String _displayName;

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  Widget _buildUsername() {
    return TextFormField(
      strutStyle: StrutStyle(forceStrutHeight: true, height: 0.9,),
      validator: (String val) {
        var res = validateEmail(val);
        if (res == false) {
          return 'invalid email';
        } else {
          return null;
        }
      },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.025),
        prefixIcon: Icon(
          Icons.email,
          color: Theme.of(context).hintColor,
          size: MediaQuery.of(context).size.width*0.07,
        ),
        labelText: 'Email Address',
        labelStyle:
        TextStyle(color: Theme.of(context).hintColor, fontSize: MediaQuery.of(context).size.width * 0.04,),
        focusedBorder: OutlineInputBorder(
          borderSide:
          BorderSide(width: 2.0, color: Theme.of(context).primaryColorDark),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide:
          BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      onChanged: (String value) {
        setState(() {
          _username = value;
        });
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      strutStyle: StrutStyle(forceStrutHeight: true, height: 0.9,),
      keyboardType: TextInputType.text,
      validator: (String val) {
        if (val.isEmpty) {
          return 'Please enter a password';
        } else if (val.trim().length < 6) {
          return "Password must be at least six characters";
        } else {
          return null;
        }
      },
      obscureText: true,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.025),
        prefixIcon: Icon(
          Icons.lock,
          color: Theme.of(context).hintColor,
          size: MediaQuery.of(context).size.width*0.07,
        ),
        labelText: 'Password',
        labelStyle:
        TextStyle(color: Theme.of(context).hintColor, fontSize: MediaQuery.of(context).size.width * 0.04),
        focusedBorder: OutlineInputBorder(
          borderSide:
          BorderSide(width: 2.0, color: Theme.of(context).primaryColorDark),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide:
          BorderSide(color: Theme.of(context).primaryColor, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      onChanged: (String value) {
        setState(() {
          _password = value;
        });
      },
    );
  }

  Widget _loginButton() {
    return (_isLoading)
        ? Container(
      margin: EdgeInsets.only(
          top: 20,
          right: MediaQuery.of(context).size.width * 0.05,
          left: MediaQuery.of(context).size.width * 0.05,
          bottom: 5),
      height: 60,
      width: 60,
      child: CircularProgressIndicator(),
    )
        : Container(
      width: MediaQuery.of(context).size.width * 0.7,
      height: MediaQuery.of(context).size.height * 0.06,
      margin: EdgeInsets.only(
          top: 20,
          right: MediaQuery.of(context).size.width * 0.05,
          left: MediaQuery.of(context).size.width * 0.05,
          bottom: 5),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 5,
        color: Theme.of(context).primaryColor,
        textColor: Colors.white,
        child: Text(
          'Sign In',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
        onPressed: () async {
          if (_formKey2.currentState.validate()) {
            setState(() {
              _isLoading = true;
            });
            try {
              final newAuth = await _authenticate();
              if (newAuth != null) {
                _newUser = await _auth.currentUser();
                if (_newUser.isEmailVerified) {
                  _displayName = _newUser.displayName;
                  SharedPreferences pref =
                  await SharedPreferences.getInstance();
                  pref.setString("logIn", "home");
                  pref.setString('userName', _newUser.email);
                  pref.setString('password', _password);
                  pref.setString(
                      'displayName', _displayName.split(" ").first);
                  Navigator.pushReplacementNamed(context, ('/homePage'));
                } else {
                  setState(() {
                    _newUser.sendEmailVerification();
                    _isError = true;
                    _isLoading = false;
                    _errorText =
                    'email not verified, check your mail for verification';
                  });
                }
              }
            } catch (e) {
              setState(() {
                _isLoading = false;
                _isError = true;
                _errorText = e.message;
              });
            }
          }
        },
      ),
    );
  }

  Widget _buildErrorText() {
    return (_isError)
        ? Container(
      margin: EdgeInsets.all(10),
      child: Text(
        _errorText,
        style: TextStyle(color: Theme.of(context).errorColor, fontSize: 12),
      ),
    )
        : Container(height: 0, width: 0);
  }

  Future<AuthResult> _authenticate() {
    return _auth
        .signInWithEmailAndPassword(email: _username, password: _password)
        .catchError(onError);
  }

  @override
  void initState() {
    super.initState();
    _isError = false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  child: Material(
                    color: Theme.of(context).primaryColor,
                    elevation: 5,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: CircleAvatar(
                            backgroundImage: AssetImage('assets/logo.png'),
                            //backgroundColor: Colors.white,
                            radius: 55,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 15, top: 15),
                          child: Text(
                            'Doorbell',
                            style: TextStyle(letterSpacing: 7.0,
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    shadowColor: Colors.black,
                    borderRadius:
                    BorderRadius.only(bottomRight: Radius.circular(100)),
                  ),
                ),
                Container(
                  child: Form(
                    key: _formKey2,
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.7,
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.only(top: 50, bottom: 20),
                          child: _buildUsername(),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.7,
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.only(
                              right: MediaQuery.of(context).size.width * 0.05,
                              left: MediaQuery.of(context).size.width * 0.05,
                              bottom: 15),
                          child: _buildPassword(),
                        ),
                        _buildErrorText(),
                        _loginButton(),
                        Container(
                          margin: EdgeInsets.only(top: 25),
                          height: 60,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text('Don\'t have an account?',
                                      style: TextStyle(
                                          fontSize: MediaQuery.of(context)
                                              .size
                                              .width *
                                              0.03)),
                                  SizedBox(
                                    width: 3,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pushNamed(context, '/signUp');
                                    },
                                    child: Text(
                                      'Get Started',
                                      style: TextStyle(
                                          fontSize: MediaQuery.of(context)
                                              .size
                                              .width *
                                              0.04,
                                          color:
                                          Theme.of(context).primaryColor),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              ForgotPassword(),
                                        ),
                                      );
                                    },
                                    child: Text('Forgot Password',
                                        style: TextStyle(
                                            fontSize: MediaQuery.of(context)
                                                .size
                                                .width *
                                                0.04,
                                            color: Theme.of(context)
                                                .primaryColor)),
                                  ))
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  onError(e) {
    print(e.message);
    setState(() {
      _isLoading = false;
      _isError = true;
      _errorText = e.message;
      if (e.message == "An internal error has occurred. [ 7: ]") {
        _errorText = "Unable to connect, no internet access";
      }
    });
    return;
  }
}
