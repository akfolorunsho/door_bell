import 'package:door_bell/ringing.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:path/path.dart' as Path;
import 'package:sqflite/sqflite.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:wc_flutter_share/wc_flutter_share.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  static String id;

  @override
  State createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  final _firestore = Firestore.instance;
  final _firebaseMessaging = FirebaseMessaging();
  PermissionStatus _permissionStatusStorage;
  PermissionStatus _permissionStatusContact;
  Iterable<Contact> _contactsOnPhone;
  List<Contacts> _requests = List<Contacts>();
  List<Contacts> _friendsOnDoorbell = List<Contacts>();
  List<Contacts> _allContacts = List<Contacts>();
  final _auth = FirebaseAuth.instance;
  List<Contacts> _addedFriends = List<Contacts>();
  TextEditingController editingController = TextEditingController();
  List<Contacts> _duplicateContacts = List<Contacts>();
  FirebaseUser _user;
  String _dpUrl;

  List<String> _numbersNotOnDoorbel = List<String>();
  List<Contacts> _addMeHeader = [
    Contacts(name: "Add me", number: "00", token: "00", type: 'request')
  ];
  List<Contacts> _friendsOnDoorbellHeader = [
    Contacts(name: "Friends on Doorbell", number: "01", token: "01", type: '')
  ];
  List<Contacts> _inviteFriends = [
    Contacts(name: "Invite Friends", number: "02", token: "02", type: '')
  ];

  final Future<Database> database = openDatabase(
    Path.join("data/data/com.bits9.door_bell/databases", 'contacts.db'),
    onCreate: (db, version) {
      return db.execute(
        "CREATE TABLE contacts(name TEXT, token TEXT, number TEXT PRIMARY KEY, urlThumbnail TEXT, type TEXT)",
      );
    },
    version: 1,
  );

  void _deleteAllContactsFromDatabase() async {
    final Database db = await database;
    db.execute("DELETE * FROM contacts");
  }


  Future<void> addContactToDatabase(Contacts contact) async {
    // Get a reference to the database.
    final Database db = await database;
    await db.insert(
      'contacts',
      contact.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Contacts>> getContactsFromDatabase(String type) async {
    // Get a reference to the database.
    final Database db = await database;
    final List<Map<String, dynamic>> maps =
    await db.query('contacts', where: 'type = ?', whereArgs: [type]);
    return List.generate(
      maps.length,
          (i) {
        return Contacts(
            name: maps[i]['name'],
            number: maps[i]['number'],
            token: maps[i]['token'],
            urlThumbnail: maps[i]['urlThumbnail'],
            type: maps[i]['type']);
      },
    );
  }

  _getContacts() async {
    _requests = await getContactsFromDatabase('request');
    _addedFriends = await getContactsFromDatabase('ringable');
    _friendsOnDoorbell = await getContactsFromDatabase('ondoorbell');
    setState(() {
      if (_requests.isEmpty && _friendsOnDoorbell.isEmpty) {
        _allContacts = _friendsOnDoorbellHeader +
            _addedFriends +
            _inviteFriends;

        _duplicateContacts = _friendsOnDoorbellHeader +
            _addedFriends +
            _inviteFriends;
      } else {
        _allContacts = _addMeHeader +
            _requests + _friendsOnDoorbell +
            _friendsOnDoorbellHeader +
            _addedFriends +
            _inviteFriends;
        _duplicateContacts = _addMeHeader +
            _requests + _friendsOnDoorbell +
            _friendsOnDoorbellHeader +
            _addedFriends +
            _inviteFriends;
      }
    });
  }

  void _getCurrentUser() async {
    FirebaseUser loggedInUser = await _auth.currentUser();
    String token = await _firebaseMessaging.getToken();
    print(token);
    try {
      QuerySnapshot users = await _firestore.collection('users').getDocuments();

      QuerySnapshot snapshots = await _firestore
          .collection('users')
          .where('phone', isEqualTo: loggedInUser.displayName
          .split(" ")
          .last)
          .getDocuments();
      String _name = loggedInUser.displayName
          .split(" ")
          .first +
          " " +
          loggedInUser.displayName.split(" ")[1];
      if (snapshots.documents.isEmpty) {
        await _firestore
            .collection('users')
            .document(loggedInUser.displayName
            .split(" ")
            .last)
            .setData({
          'token': token,
          'name': _name,
          'phone': loggedInUser.displayName
              .split(" ")
              .last
        });
      } else {
        for (var snapshot in snapshots.documents) {
          if (snapshot.data['token'] != token) {
            await _firestore
                .collection('users')
                .document(loggedInUser.displayName
                .split(" ")
                .last)
                .updateData({
              'token': token,
            });
            for (var contacts in users.documents) {
              QuerySnapshot doc = await _firestore
                  .document('users/${contacts.documentID}')
                  .collection('contacts')
                  .where('number',
                  isEqualTo: loggedInUser.displayName
                      .split(" ")
                      .last)
                  .getDocuments();
              for (var user in doc.documents) {
                user.reference.updateData({'token': token});
              }
            }
          }
        }
      }
    } catch (e) {
      print(e);
      return;
    }
  }

  Future<void> deleteContact(String number) async {
    // Get a reference to the database.
    final db = await database;
    // Remove the Contact from the database.
    await db.delete(
      'contacts',
      where: "number = ?",
      whereArgs: [number],
    );
  }

  void _populateDatabaseForTheFirstTime() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    _user = await _auth.currentUser();
    String number = _user.displayName
        .split(" ")
        .last;
    _permissionStatusContact = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.contacts);
    _permissionStatusStorage = await PermissionHandler().checkPermissionStatus(
        PermissionGroup.contacts); //check if permission is already granted
    if (_permissionStatusContact != PermissionStatus.granted) {
      // if not, request for permission
      await PermissionHandler().requestPermissions(
          [PermissionGroup.contacts, PermissionGroup.contacts]);
    }
    if (_permissionStatusStorage != PermissionStatus.granted) {
      // if not, request for permission
      await PermissionHandler().requestPermissions(
          [PermissionGroup.contacts, PermissionGroup.storage]);
    }
    _contactsOnPhone = await ContactsService.getContacts(withThumbnails: false);
    List<Contacts> _myContactsOnline = List<Contacts>();
    QuerySnapshot _doorbellUsers =
    await _firestore.collection('users').getDocuments();
    QuerySnapshot _addedContacts = await _firestore
        .collection('users')
        .document(number)
        .collection('contacts')
        .getDocuments();
    for (var added in _addedContacts.documents) {
      _myContactsOnline.add(Contacts(
          name: added.data["name"],
          number: added.data['number'],
          token: added.data['token'],
          urlThumbnail: added.data['urlThumbnail']));
    }
    List<Contacts> _onlineContacts = List<Contacts>();
    for (var onlineCon in _doorbellUsers.documents) {
      _onlineContacts.add(Contacts(
          name: onlineCon.data['name'],
          number: onlineCon.data['phone'],
          token: onlineCon.data['phone'],
          urlThumbnail: onlineCon.data['urlThumbnail']));
    }
    for (var contact in _contactsOnPhone) {
      if (contact.phones.isNotEmpty) {
        contact.phones.forEach((item) {
          String correctedNumber;
          if (item.value
              .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
              .split("")
              .length >
              4) {
            correctedNumber = (item.value
                .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
                .split("")[0] +
                item.value
                    .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
                    .split("")[1] +
                item.value
                    .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
                    .split("")[2] +
                item.value
                    .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
                    .split("")[3] ==
                '+234' &&
                item.value
                    .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
                    .length ==
                    14)
                ? "0" +
                item.value
                    .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
                    .split("")[4] +
                item.value
                    .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
                    .split("")[5] +
                item.value
                    .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
                    .split("")[6] +
                item.value
                    .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
                    .split("")[7] +
                item.value
                    .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
                    .split("")[8] +
                item.value.replaceAll(new RegExp(r"\s+\b|\b\s"), "").split(
                    "")[9] +
                item.value.replaceAll(new RegExp(r"\s+\b|\b\s"), "").split(
                    "")[10] +
                item.value.replaceAll(new RegExp(r"\s+\b|\b\s"), "").split(
                    "")[11] +
                item.value.replaceAll(new RegExp(r"\s+\b|\b\s"), "").split(
                    "")[12] +
                item.value.replaceAll(new RegExp(r"\s+\b|\b\s"), "").split(
                    "")[13]
                : item.value.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
          }
          Contacts _ondoorbell = _onlineContacts.singleWhere(
                  (item) => item.number == correctedNumber, orElse: (() {
            return null;
          }));
          if (_ondoorbell != null && _ondoorbell.number != number) {
            if (!_myContactsOnline.contains(_ondoorbell)) {
              Contacts _onDoorbellWithType = Contacts(
                  name: _ondoorbell.name,
                  token: _ondoorbell.token,
                  number: _ondoorbell.number,
                  urlThumbnail: _ondoorbell.urlThumbnail,
                  type: 'ondoorbell');
              addContactToDatabase(_onDoorbellWithType);
            }
          } else if (_ondoorbell.number != number) {
            _numbersNotOnDoorbel.add(correctedNumber);
          }
        });
      }
    }
    print(_numbersNotOnDoorbel);
    preferences.setStringList("remainingNumber", _numbersNotOnDoorbel);
  }

  void _checkIfFirstLaunchAndSetFalse() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool _check = preferences.getBool('firstLaunch');
    if (_check == null || _check == true) {
      _deleteAllContactsFromDatabase();
      _populateDatabaseForTheFirstTime();
      preferences.setBool('firstLaunch', false);
    }
  }

  void _listenForChangeInContactsOnline() async {
    FirebaseUser _user = await _auth.currentUser();
    String phoneNumber = _user.displayName
        .split(" ")
        .last;
    _firestore
        .collection('users')
        .document(phoneNumber)
        .collection('contacts')
        .snapshots()
        .listen(
          (data) {
        if (this.mounted) {
          for (var dat in data.documents) {
            addContactToDatabase(
              Contacts(
                  name: dat.data['name'],
                  token: dat.data['token'],
                  number: dat.data['number'],
                  urlThumbnail: dat.data['urlThumbnail'],
                  type: 'ringable'),
            );
          }
          _getContacts();
        }
      },
    );
  }

  void _listenForChangeInRequestsOnline() async {
    FirebaseUser _user = await _auth.currentUser();
    String phoneNumber = _user.displayName
        .split(" ")
        .last;
    _firestore
        .collection('users')
        .document(phoneNumber)
        .collection('requests')
        .snapshots()
        .listen(
          (data) {
        if (this.mounted) {
          for (var dat in data.documents) {
            addContactToDatabase(
              Contacts(
                  name: dat.data['senderName'],
                  token: dat.data['token'],
                  number: dat.data['senderNumber'],
                  urlThumbnail: dat.data['urlThumbnail'],
                  type: 'request'),
            );
          }
          _getContacts();
        }
      },
    );
  }

  void _listenForChangeInUsersOnline() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    _numbersNotOnDoorbel = pref.getStringList("remainingNumber");
    FirebaseUser _user = await _auth.currentUser();
    String phoneNumber = _user.displayName
        .split(" ")
        .last;
    _firestore
        .collection('users')
        .snapshots()
        .listen(
          (data) {
        if (this.mounted) {
          for (var dat in data.documents) {
            if (dat.data['phone'] != phoneNumber &&
                _numbersNotOnDoorbel.contains(dat.data['phone'])) {
              addContactToDatabase(
                Contacts(
                    name: dat.data['name'],
                    token: dat.data['token'],
                    number: dat.data['phone'],
                    urlThumbnail: dat.data['urlThumbnail'],
                    type: 'ondoorbell'),
              );
              _getContacts();
            }
          }
        }
      },
    );
  }

  void filterSearchResults(String query) {
    if (query != "") {
      print("query time");
      var _a = query.substring(1);
      var _firstLetter = query[0].toUpperCase();
      var _realQuery = _firstLetter + _a;

      List<Contacts> dummyListData = new List<Contacts>();
      for (var friend in _duplicateContacts) {
        if (friend.name.contains(_realQuery)) {
          dummyListData.add(friend);
        }
      }
      setState(() {
        _allContacts = dummyListData;
      });
    } else {
      setState(() {
        _allContacts = _duplicateContacts;
      });
    }
  }

  Widget _buildSearchTextField() {
    return Container(
      margin: EdgeInsets.only(left: MediaQuery
          .of(context)
          .size
          .width * .021),
      height: MediaQuery
          .of(context)
          .size
          .height * 0.04,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Theme
            .of(context)
            .primaryColorLight,
      ),
      child: TextField(
        onChanged: (value) {
          filterSearchResults(value);
        },
        cursorColor: Color(0XF757575),
        controller: editingController,
        decoration: InputDecoration(
          fillColor: Theme
              .of(context)
              .primaryColorLight,
          labelText: "Search",
          hasFloatingPlaceholder: false,
          labelStyle: TextStyle(
              color: Theme.of(context).hintColor,
              fontSize: MediaQuery
                  .of(context)
                  .size
                  .width * 0.04),
          prefixIcon: Icon(
            Icons.search,
            color: Theme.of(context).hintColor,
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0.0, color: Colors.transparent),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent, width: 0),
          ),
        ),
      ),
    );
  }

  _ringFriend(String number, String token, String urlThumbnail) async {
    try {
      _user = await _auth.currentUser();
      String _userNumber = _user.displayName
          .split(" ")
          .last;
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => Ringing(number, token, urlThumbnail),
        ),
      );
      await _firestore
          .collection('users')
          .document(_userNumber)
          .updateData({'reply': 'Bell Ringing'});
    } catch (e) {
      print(e);
    }
  }

  _acceptRequest(int index) async {
    FirebaseUser _user = await _auth.currentUser();
    String _name = _user.displayName
        .split(" ")
        .first +
        " " +
        _user.displayName.split(" ")[1];
    String _phoneNumber = _user.displayName
        .split(" ")
        .last;
    String _thumbnail = _user.photoUrl;
    String token = await _firebaseMessaging.getToken();
    try {
      //Add data for the person that is requesting
      await _firestore
          .collection('users')
          .document(_allContacts[index].number)
          .collection('contacts')
          .document(_phoneNumber)
          .setData({
        'name': _name,
        'token': token,
        "number": _phoneNumber,
        'urlThumbnail': _thumbnail
      });

      //Add data for the person that is adding request's contact
      await _firestore
          .collection('users')
          .document(_phoneNumber)
          .collection('contacts')
          .document(_allContacts[index].number)
          .setData({
        'name': _allContacts[index].name,
        'number': _allContacts[index].number,
        'token': _allContacts[index].token,
        'urlThumbnail': _allContacts[index].urlThumbnail
      });
      await _firestore
          .collection('users')
          .document(_phoneNumber)
          .collection('requests')
          .document(_allContacts[index].number)
          .delete();
      await deleteContact(_allContacts[index].number);
      setState(() {
        _allContacts.removeAt(index);
      });
    } catch (e) {
      Fluttertoast.showToast(msg: e, toastLength: Toast.LENGTH_LONG);
      return;
    }
  }

  _sendRequest(int index) async {
    FirebaseUser _user = await _auth.currentUser();
    String name = _user.displayName
        .split(" ")
        .first +
        " " +
        _user.displayName.split(' ')[1];
    String number = _user.displayName
        .split(" ")
        .last;
    String token = await _firebaseMessaging.getToken();
    await _firestore
        .collection('users')
        .document(_allContacts[index].number)
        .collection('requests')
        .document(number)
        .setData(
      {
        'senderName': name,
        'senderNumber': number,
        'token': token,
        'urlThumbnail': _user.photoUrl
      },
    );
    await deleteContact(_allContacts[index].number);
    setState(() {
      _allContacts.removeAt(index);
    });
  }

  _getDp() async {
    FirebaseUser _user = await _auth.currentUser();
    String url = _user.photoUrl;
    setState(() {
      _dpUrl = url;
    });
  }

  Future<String> _getDpFullImage(String number) async {
    print(number);
    String urlFullImage;
    final StorageReference storageRef = FirebaseStorage.instance.ref().child(
        number);
    urlFullImage = (await storageRef.getDownloadURL()).toString();
    print('getDpFullImage Testing: ' + urlFullImage);
    return urlFullImage;
  }

  void _listenForChangeInRinger() async {
    FirebaseUser _user = await _auth.currentUser();
    String phoneNumber = _user.displayName
        .split(" ")
        .last;
    _firestore
        .collection('users')
        .document(phoneNumber)
        .collection('ringer')
        .snapshots()
        .listen(
          (data) {
        if (this.mounted) {
          if (data.documents.isNotEmpty) {
            Navigator.pushNamed(context, "/byTheDoor");
          }
        }
      },
    );
  }

  @override
  void initState() {
    _checkIfFirstLaunchAndSetFalse();
    _getContacts();
    _listenForChangeInContactsOnline();
    _listenForChangeInRequestsOnline();
    _listenForChangeInUsersOnline();
    _listenForChangeInRinger();
    _getDp();
    _getCurrentUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme
              .of(context)
              .primaryColor,
          leading: (_dpUrl != null)
              ? Container(
            padding: EdgeInsets.only(
                left: MediaQuery
                    .of(context)
                    .size
                    .width * .03,
                top: MediaQuery
                    .of(context)
                    .size
                    .width * .015,
                bottom: MediaQuery
                    .of(context)
                    .size
                    .width * .015),
            child: GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/settingsTab');
              },
              child: CircleAvatar(
                backgroundImage: NetworkImage(_dpUrl),
                radius: 10,
              ),
            ),
          )
              : Container(
            margin: EdgeInsets.only(
                left: MediaQuery
                    .of(context)
                    .size
                    .width * .03),
            child: GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/settingsTab');
              },
              child: CircleAvatar(
                backgroundColor: Theme
                    .of(context)
                    .primaryColorLight,
                radius: MediaQuery
                    .of(context)
                    .size
                    .width * 0.06,
                child: Icon(
                  Icons.person,
                  color: Color(0XFFBDBDBD),
                  size: MediaQuery
                      .of(context)
                      .size
                      .width * 0.1,
                ),
              ),
            ),
          ),
          title: _buildSearchTextField(),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.share, color: Theme
                    .of(context)
                    .primaryColorLight),
                onPressed: () {
                  WcFlutterShare.share(
                      sharePopupTitle: 'Share Doorbell',
                      subject:
                      'Click the link below to install Doorbell app',
                      text:
                      'Click the link below to install Doorbell app: link',
                      mimeType: 'text/plain');
                },
                padding: EdgeInsets.all(MediaQuery
                    .of(context)
                    .size
                    .width * .0))
          ],
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                itemCount: _allContacts.length,
                itemBuilder: (context, index) {
                  return (_allContacts[index].name == 'Add me' ||
                      _allContacts[index].name == 'Friends on Doorbell')
                      ? Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery
                            .of(context)
                            .size
                            .height * .005,
                        right: MediaQuery
                            .of(context)
                            .size
                            .width * .032,
                        left: MediaQuery
                            .of(context)
                            .size
                            .width * .032,
                        bottom:
                        MediaQuery
                            .of(context)
                            .size
                            .height * .005),
                    child: Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery
                              .of(context)
                              .size
                              .height * .015,
                          bottom:
                          MediaQuery
                              .of(context)
                              .size
                              .height * .006),
                      child: Text(
                        _allContacts[index].name,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0XFF212121)),
                      ),
                    ),
                  )
                      : (_allContacts[index].name == 'Invite Friends')
                      ? Padding(
                    padding: const EdgeInsets.only(top: 30, right: 25, left: 25),
                    child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            _allContacts[index].name,
                            style: TextStyle(
                              fontSize: MediaQuery.of(context).size.width * .05,
                                fontWeight: FontWeight.bold,
                                color: Color(0XFF212121)),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: IconButton(icon: Icon(Icons.share),
                              onPressed: () {
                                WcFlutterShare.share(
                                    sharePopupTitle: 'Invite  Friends to Doorbell',
                                    subject:
                                    'Install Doorbell app to notify me when you are in my house of office',
                                    text:
                                    'Click the link below to install Doorbell app: link',
                                    mimeType: 'text/plain');
                              }),
                        )
                      ],
                    ),
                  )
                      : Card(
                    shape: (_requests.isEmpty && _friendsOnDoorbell.isEmpty)
                        ? (index == 1)
                        ? RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(5.0),
                            topLeft: Radius.circular(5.0)))
                        : (index == (_allContacts.length - 2))
                        ? RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(5.0),
                            bottomLeft: Radius.circular(5.0)))
                        : RoundedRectangleBorder(
                      borderRadius:
                      BorderRadius.all(Radius.zero),
                    )
                        : (index == 1)
                        ? RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(5.0),
                            topLeft: Radius.circular(5.0)))
                        : (index ==
                        (_requests.length + _friendsOnDoorbell.length) ||
                        index == (_allContacts.length - 1))
                        ? RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(5.0),
                            bottomLeft: Radius.circular(5.0)))
                        : (index == (_requests.length + _friendsOnDoorbell
                        .length + 2))
                        ? RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(5.0),
                            topLeft: Radius.circular(5.0)))
                        : RoundedRectangleBorder(
                      borderRadius:
                      BorderRadius.all(Radius.zero),
                    ),
                    margin: EdgeInsets.symmetric(
                      vertical:
                      MediaQuery
                          .of(context)
                          .size
                          .height * .0005,
                      horizontal:
                      MediaQuery
                          .of(context)
                          .size
                          .width * .032,
                    ),

                    child: ((_requests.isNotEmpty ||
                        _friendsOnDoorbell.isNotEmpty) && index <=
                            (_requests.length + _friendsOnDoorbell.length))
                        ? Dismissible(
                      background: Container(color: Colors.red),
                      key: Key(_allContacts[index].number),
                      onDismissed: (direction) {
                        setState(() {
                          Contacts deletedItem = _allContacts.removeAt(index);
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              content: Text(
                                  "Remove ${_allContacts[index].name} "),
                              action: SnackBarAction(
                                  label: "UNDO",
                                  onPressed: () =>
                                      setState(() =>
                                          _allContacts.insert(index,
                                              deletedItem),) // this is what you needed
                              ),
                            ),
                          );
                        });
                      },
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(
                            vertical:
                            MediaQuery
                                .of(context)
                                .size
                                .height * .005,
                            horizontal:
                            MediaQuery
                                .of(context)
                                .size
                                .width * .032),
                        leading: (_allContacts[index].urlThumbnail != null)
                            ? GestureDetector(
                          onTap: () async {
                            String number = _allContacts[index].number;
                            String _urlFullImage = await _getDpFullImage(
                                number);
                            showDialog(context: context,
                                builder: (context) =>
                                    CustomDialog(urlFull: _urlFullImage,));
                          },
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(
                                _allContacts[index].urlThumbnail),
                            radius:
                            MediaQuery
                                .of(context)
                                .size
                                .width *
                                0.06,
                          ),
                        )
                            : CircleAvatar(
                          backgroundColor: Colors.black12,
                          radius:
                          MediaQuery
                              .of(context)
                              .size
                              .width *
                              0.06,
                          child: Icon(
                            Icons.person,
                            color: Colors.white,
                            size: MediaQuery
                                .of(context)
                                .size
                                .width *
                                0.1,
                          ),
                        ),
                        title: Text(
                          _allContacts[index].name,
                          style: TextStyle(
                              fontSize:
                              MediaQuery
                                  .of(context)
                                  .size
                                  .width * .035,
                              fontWeight: FontWeight.normal,
                              color: Color(0XFF212121)),
                        ),
                        trailing: Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.03,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: FlatButton.icon(
                              color: Theme
                                  .of(context)
                                  .primaryColor,
                              onPressed: () {
                                if (_allContacts[index].type ==
                                    'request') {
                                  _acceptRequest(index);
                                } else {
                                  _sendRequest(index);
                                }
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(
                                    MediaQuery
                                        .of(context)
                                        .size
                                        .width *
                                        2),
                              ),
                              icon: Icon(Icons.person_add,
                                  color: Colors.white,
                                  size: MediaQuery
                                      .of(context)
                                      .size
                                      .height *
                                      0.019),
                              label: (_allContacts[index].type ==
                                  'request')
                                  ? Text(
                                'Accept',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.white),
                              )
                                  : Text(
                                'Add       ',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.white),
                              )
                          ),
                        ),
                      ),
                    )
                        : ListTile(
                      contentPadding: EdgeInsets.symmetric(
                          vertical:
                          MediaQuery
                              .of(context)
                              .size
                              .height * .005,
                          horizontal:
                          MediaQuery
                              .of(context)
                              .size
                              .width * .032),
                      leading: (_allContacts[index].urlThumbnail != null)
                          ? GestureDetector(
                        onTap: () async {
                          String number = _allContacts[index].number;
                          String _urlFullImage = await _getDpFullImage(number);
                          showDialog(context: context,
                              builder: (context) =>
                                  CustomDialog(urlFull: _urlFullImage,));
                        },
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                              _allContacts[index].urlThumbnail),
                          radius:
                          MediaQuery
                              .of(context)
                              .size
                              .width *
                              0.06,
                        ),
                      )
                          : CircleAvatar(
                        backgroundColor: Colors.black12,
                        radius:
                        MediaQuery
                            .of(context)
                            .size
                            .width *
                            0.06,
                        child: Icon(
                          Icons.person,
                          color: Colors.white,
                          size: MediaQuery
                              .of(context)
                              .size
                              .width *
                              0.1,
                        ),
                      ),
                      title: Text(
                        _allContacts[index].name,
                        style: TextStyle(
                            fontSize:
                            MediaQuery
                                .of(context)
                                .size
                                .width * .035,
                            fontWeight: FontWeight.normal,
                            color: Color(0XFF212121)),
                      ),
                      trailing: Container(
                        height: MediaQuery
                            .of(context)
                            .size
                            .height * 0.03,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                        ),
                        child: FlatButton.icon(
                            color: Theme
                                .of(context)
                                .primaryColor,
                            onPressed: () {
                              if (_allContacts[index].type ==
                                  'request') {
                                _acceptRequest(index);
                              } else if (_allContacts[index].type ==
                                  'ringable') {
                                _ringFriend(
                                    _allContacts[index].number,
                                    _allContacts[index].token,
                                    _allContacts[index].urlThumbnail);
                              } else {
                                _sendRequest(index);
                              }
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(
                                  MediaQuery
                                      .of(context)
                                      .size
                                      .width *
                                      2),
                            ),
                            icon: (_allContacts[index].type ==
                                'request' ||
                                _allContacts[index].type ==
                                    'ondoorbell')
                                ? Icon(Icons.person_add,
                                color: Colors.white,
                                size: MediaQuery
                                    .of(context)
                                    .size
                                    .height *
                                    0.019)
                                : (_allContacts[index].type == 'ringable')
                                ? Icon(Icons.notifications_active,
                                color: Colors.white,
                                size: MediaQuery
                                    .of(context)
                                    .size
                                    .height *
                                    0.019)
                                : Icon(Icons.share,
                                color: Colors.white,
                                size: MediaQuery
                                    .of(context)
                                    .size
                                    .height *
                                    0.019),
                            label: (_allContacts[index].type ==
                                'request')
                                ? Text(
                              'Accept',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.white),
                            )
                                : (_allContacts[index].type ==
                                'ringable')
                                ? Text(
                              'Bell       ',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.white),
                            )
                                : Text(
                              'Add       ',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.white),
                            )
                        ),
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Contacts {
  String name;
  String token;
  String number;
  String urlThumbnail;
  String type;

  Contacts({this.name, this.token, this.number, this.urlThumbnail, this.type});

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'token': token,
      'number': number,
      'urlThumbnail': urlThumbnail,
      'type': type
    };
  }
}


class CustomDialog extends StatelessWidget {
  final String urlFull;

  CustomDialog({@required this.urlFull});


  @override
  Widget build(BuildContext context) {
    return Dialog(shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8.0),),
        child: Image(image: NetworkImage(urlFull)));
  }
}
