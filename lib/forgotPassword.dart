import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ForgotPassword extends StatefulWidget {
  static String id;
  @override
  _ForgotPassword createState() => _ForgotPassword();
}

class _ForgotPassword extends State<ForgotPassword> {
  final GlobalKey<FormState> _formKey3 = GlobalKey<FormState>();

  final _auth = FirebaseAuth.instance;
  String _email;

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          // primaryColor: Colors.white,
          highlightColor: Colors.white,
          hintColor: Colors.white,
          cursorColor: Theme.of(context).accentColor),
      home: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
            body: SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  child: Material(
                    color: Theme.of(context).primaryColor,
                    elevation: 5,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: CircleAvatar(
                            backgroundImage: AssetImage('assets/logo.png'),
                            //backgroundColor: Colors.white,
                            radius: 55,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 15, top: 15),
                          child: Text(
                            'Password Reset',
                            style: TextStyle(letterSpacing: 7.0,
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    shadowColor: Colors.black,
                    borderRadius:
                    BorderRadius.only(bottomRight: Radius.circular(100)),
                  ),
                ),
                Container(
                    width: MediaQuery.of(context).size.width * 0.7,
                    height: MediaQuery.of(context).size.height * 0.06,
                    margin: EdgeInsets.only(top: 50, bottom: 10),
                    child: Form(
                      key: _formKey3,
                      child: TextFormField(
                        strutStyle: StrutStyle(forceStrutHeight: true, height: 0.9,),
                        keyboardType: TextInputType.emailAddress,
                        validator: (String val) {
                          var res = validateEmail(val);
                          if (res == false) {
                            return 'Invalid email';
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.025),
                          prefixIcon: Icon(
                            Icons.email,
                            color: Theme.of(context).hintColor,
                            size: 25,
                          ),
                          labelText: 'Enter Email',
                          labelStyle: TextStyle(
                              color: Theme.of(context).hintColor,
                              fontSize: MediaQuery.of(context).size.width * 0.04,),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 2.0,
                                color: Theme.of(context).primaryColorDark),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).primaryColor, width: 1.0),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        onChanged: (String value) {
                          setState(() {
                            _email = value;
                          });
                        },
                      ),
                    )),
                Container(
                  width: MediaQuery.of(context).size.width * 0.7,
                  margin: EdgeInsets.only(top: 30),
                  height: MediaQuery.of(context).size.height * 0.06,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    color: Theme.of(context).primaryColor,
                    child: Text('Reset Password',
                        style: TextStyle(fontSize: 20, color: Colors.white)),
                    elevation: 5,
                    onPressed: () async {
                      if (_formKey3.currentState.validate()) {
                        await _auth
                            .sendPasswordResetEmail(email: _email)
                            .catchError(onError);
                        Fluttertoast.showToast(
                            msg: "Reset Link has Been Sent to Your Email",
                            toastLength: Toast.LENGTH_LONG);
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        )),
      ),
    );
  }

  onError(e) {
    Fluttertoast.showToast(msg: "Error", toastLength: Toast.LENGTH_LONG);
  }
}
