import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class ByTheDoor extends StatefulWidget {
  static String id;

  @override
  State<StatefulWidget> createState() => _ByTheDoor();
}

class _ByTheDoor extends State<ByTheDoor> {
  var _uri;

  final _firestore = Firestore.instance;
  final _auth = FirebaseAuth.instance;
  String ringerName;
  String ringerNumber;
  String ringerToken;

  _atHome() async {
    await _firestore
        .collection('users')
        .document(ringerNumber)
        .updateData({'reply': 'Am Coming'});
    _deleteRingers();
  }

  _notAtHome() async {
    await _firestore
        .collection('users')
        .document(ringerNumber)
        .updateData({'reply': 'Not at Home'});
    _deleteRingers();
  }

  @override
  void initState() {
    _getDp();
    _getRinger();
    super.initState();
  }


  _getRinger() async {
    FirebaseUser _user = await _auth.currentUser();
    String phoneNumber = _user.displayName.split(" ").last;

    QuerySnapshot snapshot = await _firestore
        .collection('users')
        .document(phoneNumber)
        .collection('ringer')
        .getDocuments();

    for (var document in snapshot.documents) {
      setState(() {
        ringerName = document.data['ringerName'];
        ringerNumber = document.data['ringerNumber'];
        ringerToken = document.data['ringerToken'];
      });
    }
  }

  _deleteRingers() async {
    FirebaseUser _user = await _auth.currentUser();
    String phoneNumber = _user.displayName.split(" ").last;
    QuerySnapshot snapshot = await _firestore
        .collection('users')
        .document(phoneNumber)
        .collection('ringer')
        .getDocuments();
    for (var document in snapshot.documents) {
      await _firestore
          .collection('users')
          .document(phoneNumber)
          .collection('ringer')
          .document(document.documentID)
          .delete();
    }
    Navigator.pushNamed(context, "/homePage");
  }

  _getDp() async {
    final ref = FirebaseStorage.instance.ref().child(ringerNumber);
    var url = await ref.getDownloadURL();
    print('my url' + url);
    setState(() {
      _uri = url;
    });
    print("uri" + _uri);
  }

  @override
  void dispose() {
    _deleteRingers();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              _deleteRingers();
            },
          ),
          backgroundColor: Theme.of(context).primaryColor),
      body: SafeArea(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * 0.5,
                height: MediaQuery.of(context).size.width * 0.5,
                margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height*.02, top: MediaQuery.of(context).size.height*.2,),
                child: ClipOval(
                  child: (_uri != null)
                      ? Image(
                          fit: BoxFit.cover,
                          image: NetworkImage(_uri),
                        )
                      : Image.asset(
                          'assets/logo.png',
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width * 0.3,
                          height: MediaQuery.of(context).size.width * 0.3,
                        ),
                ),
              ),
              Text((ringerName != null) ? ringerName.toUpperCase() : 'Someone',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: MediaQuery.of(context).size.width * 0.06)),
              Container(
                margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height*.1),
                child: Text("is at the door",
                    style: TextStyle(color: Colors.white, fontSize: 20)),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        GestureDetector(
                          onTap: _atHome,
                          child: CircleAvatar(
                            child: Icon(
                              Icons.check,
                              color: Colors.white,
                              size: 30,
                            ),
                            radius: 30,
                            backgroundColor: Colors.green,
                          ),
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height*.01,),
                        Text(
                          "IN",
                          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.06, color: Colors.white),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.4,
                    ),
                    Column(
                      children: <Widget>[
                        GestureDetector(
                          onTap: _notAtHome,
                          child: CircleAvatar(
                            child: Icon(
                              Icons.close,
                              color: Colors.white,
                              size: 30,
                            ),
                            radius: 30,
                            backgroundColor: Colors.red,
                          ),
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height*.01,),
                        Text(
                          "OUT",
                          style: TextStyle(fontSize: MediaQuery.of(context).size.width*.06, color: Colors.white,),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
