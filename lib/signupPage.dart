import 'package:app_settings/app_settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignupPage extends StatefulWidget {
  static String id;

  @override
  _SignupPage createState() => _SignupPage();
}

class _SignupPage extends State<SignupPage> {
  // String _passwordError;
  // String _emailError;
  final _auth = FirebaseAuth.instance;
  final _firestore = Firestore.instance;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _firstName;
  String _lastName;
  static String _email;
  String _phoneNumber;
  String _password;
  bool _isLoading = false;
  bool _isError = false;
  String _errorText;
  bool _passwordVisibility;

  Widget _buildFirstName() {
    return TextFormField(
      //style: TextStyle(fontSize: MediaQuery.of(context).size.height*0.022,),
      strutStyle: StrutStyle(forceStrutHeight: true, height: 0.9,),
      textCapitalization: TextCapitalization.words,
      keyboardType: TextInputType.text,
      validator: (String val) {
        if (val.isEmpty) {
          return 'Input First Name';
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.025),
        hasFloatingPlaceholder: false,
        prefixIcon: Icon(
          Icons.person_outline,
          color: Theme.of(context).hintColor,
          size: 25,
        ),
        labelText: 'First Name',
        labelStyle: TextStyle(
            color: Theme.of(context).hintColor,
            fontSize: MediaQuery.of(context).size.width * 0.04),
        focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(width: 2.0, color: Theme.of(context).primaryColorDark),
            borderRadius: BorderRadius.circular(10)),
        enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 1),
            borderRadius: BorderRadius.circular(10)),
      ),
      onChanged: (String value) {
        setState(() {
          _firstName = value;
        });
      },
    );
  }

  Widget _buildLastName() {
    return TextFormField(
      strutStyle: StrutStyle(forceStrutHeight: true, height: 0.9,),
      textCapitalization: TextCapitalization.words,
      keyboardType: TextInputType.text,
      validator: (String val) {
        if (val.isEmpty) {
          return 'Last Name';
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.025),
        hasFloatingPlaceholder: false,
        prefixIcon: Icon(
          Icons.person_outline,
          color: Theme.of(context).hintColor,
          size: 25,
        ),
        labelText: 'Last Name',
        labelStyle: TextStyle(
            color: Theme.of(context).hintColor,
            fontSize: MediaQuery.of(context).size.width * 0.04),
        focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(width: 2.0, color: Theme.of(context).primaryColorDark),
            borderRadius: BorderRadius.circular(10)),
        enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            borderRadius: BorderRadius.circular(10)),
      ),
      onChanged: (String value) {
        setState(() {
          _lastName = value;
        });
      },
    );
  }

  Widget _buildMobileNumber() {
    return TextFormField(
      strutStyle: StrutStyle(forceStrutHeight: true, height: 0.9,),
      keyboardType: TextInputType.number,
      validator: (String val) {
        String pattern = r'(^([0][7-9])[0-9]{9}$)';
        RegExp regex = new RegExp(pattern);
        if (val.isEmpty) {
          return 'Input your phone number';
        } else if (!regex.hasMatch(val)) {
          return 'invalid number: should start will \'0\' and contain 11 digits';
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.025),
        hasFloatingPlaceholder: false,
        prefixIcon: Icon(
          Icons.phone_android,
          color: Theme.of(context).hintColor,
          size: 25,
        ),
        labelText: 'Mobile Number',
        labelStyle: TextStyle(
            color: Theme.of(context).hintColor,
            fontSize: MediaQuery.of(context).size.width * 0.04),
        focusedBorder: OutlineInputBorder(
          borderSide:
              BorderSide(width: 2.0, color: Theme.of(context).primaryColorDark),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      onChanged: (String value) {
        setState(() {
          _phoneNumber = value;
        });
      },
    );
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  Widget _buildEmail() {
    return TextFormField(
      strutStyle: StrutStyle(forceStrutHeight: true, height: 0.9,),
      keyboardType: TextInputType.emailAddress,
      validator: (String val) {
        var res = validateEmail(val);
        if (res == false) {
          return 'invalid email';
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.025),
        hasFloatingPlaceholder: false,
        prefixIcon: Icon(
          Icons.email,
          color: Theme.of(context).hintColor,
          size: 25,
        ),
        labelText: 'Email Address',
        labelStyle: TextStyle(
            color: Theme.of(context).hintColor,
            fontSize: MediaQuery.of(context).size.width * 0.04),
        focusedBorder: OutlineInputBorder(
          borderSide:
              BorderSide(width: 2.0, color: Theme.of(context).primaryColorDark),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      onChanged: (String value) {
        setState(() {
          _email = value;
        });
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      strutStyle: StrutStyle(forceStrutHeight: true, height: 0.9,),
      keyboardType: TextInputType.text,
      validator: (String val) {
        if (val.isEmpty) {
          return 'Please enter a password';
        } else {
          return null;
        }
      },
      obscureText: _passwordVisibility,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.025),
        hasFloatingPlaceholder: false,
        prefixIcon: Icon(
          Icons.lock,
          color: Theme.of(context).hintColor,
          size: 25,
        ),
        suffixIcon: IconButton(
          icon: Icon(
              (_passwordVisibility) ? Icons.visibility_off : Icons.visibility,
              color: Theme.of(context).hintColor),
          onPressed: () {
            setState(() {
              _passwordVisibility = !_passwordVisibility;
            });
          },
        ),
        labelText: 'Password',
        labelStyle: TextStyle(
            color: Theme.of(context).hintColor,
            fontSize: MediaQuery.of(context).size.width * 0.04),
        focusedBorder: OutlineInputBorder(
          borderSide:
              BorderSide(width: 2.0, color: Theme.of(context).primaryColorDark),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      onChanged: (String value) {
        setState(() {
          _password = value;
        });
      },
    );
  }

  Widget _buildErrorText() {
    return (_isError)
        ? Container(
            margin: EdgeInsets.all(10),
            child: Text(
              _errorText,
              style: TextStyle(color: Theme.of(context).errorColor, fontSize: 12),
            ),
          )
        : Container(height: 0, width: 0);
  }

  Widget _getStartedbutton() {
    return (_isLoading)
        ? Container(width: 60, height: 60, child: CircularProgressIndicator())
        : Container(
            width: MediaQuery.of(context).size.width * 0.7,
            height: MediaQuery.of(context).size.height * 0.06,
            margin: EdgeInsets.only(
                right: MediaQuery.of(context).size.width * 0.05,
                left: MediaQuery.of(context).size.width * 0.05,
                bottom: 5),
            child: RaisedButton(
              elevation: 5.0,
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                'Get Started',
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  setState(() {
                    _isLoading = true;
                  });
                  try {
                    List<String> users = new List();
                    QuerySnapshot snap =
                        await _firestore.collection('users').getDocuments();
                    for (var sn in snap.documents) {
                      users.add(sn.documentID);
                    }
                    if (users.contains(_phoneNumber)) {
                      setState(() {
                        _isLoading = false;
                        _isError = true;
                        _errorText =
                            'Phone number already used by another user';
                      });
                    } else {
                      final newAuth =
                          await _auth.createUserWithEmailAndPassword(
                              email: _email, password: _password);
                      if (newAuth != null) {
                        final _newUser = await _auth.currentUser();
                        await _newUser.sendEmailVerification();
                        showDialog(
                          context: context,
                          builder: (BuildContext context) => CustomDialog(
                            title: "Verification Email",
                            description:
                                "A verification Email has been sent to your Email, follow the link to activate your account",
                            buttonText: "Ok",
                          ),
                        );
                        UserUpdateInfo info = UserUpdateInfo();
                        info.displayName =
                            _lastName + " " + _firstName + " " + _phoneNumber;
                        _newUser.updateProfile(info);
                        _newUser.reload();

                        Navigator.pushReplacementNamed(context, '/login');
                      }
                    }
                  } catch (e) {
                    print(e);
                    setState(
                      () {
                        _isLoading = false;
                        _isError = true;
                        _errorText = e.message;
                        if (e.message ==
                            "An internal error has occurred. [ 7: ]") {
                          _errorText = "Unable to connect, no internet access";
                        }
                      },
                    );
                  }
                }
              },
            ),
          );
  }

  @override
  void initState() {
    _passwordVisibility = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  child: Material(
                    color: Theme.of(context).primaryColor,
                    elevation: 5,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: CircleAvatar(
                            backgroundImage: AssetImage('assets/logo.png'),
                            //backgroundColor: Colors.white,
                            radius: 55,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 15, top: 15),
                          child: Text(
                            'Doorbell',
                            style: TextStyle(letterSpacing: 7.0,
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    shadowColor: Colors.black,
                    borderRadius:
                        BorderRadius.only(bottomRight: Radius.circular(100)),
                  ),
                ),
                Container(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.7,
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.only(
                              right: MediaQuery.of(context).size.width * 0.05,
                              left: MediaQuery.of(context).size.width * 0.05,
                              top: 50,
                              bottom: 20),
                          child: _buildFirstName(),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.7,
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.only(
                              right: MediaQuery.of(context).size.width * 0.05,
                              left: MediaQuery.of(context).size.width * 0.05,
                              bottom: 20),
                          child: _buildLastName(),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.7,
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.only(
                              right: MediaQuery.of(context).size.width * 0.05,
                              left: MediaQuery.of(context).size.width * 0.05,
                              bottom: 20),
                          child: _buildMobileNumber(),
                        ),
                        Container(
                          padding: EdgeInsets.zero,
                          width: MediaQuery.of(context).size.width * 0.7,
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.only(
                              right: MediaQuery.of(context).size.width * 0.05,
                              left: MediaQuery.of(context).size.width * 0.05,
                              bottom: 20),
                          child: _buildEmail(),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.7,
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.only(
                              right: MediaQuery.of(context).size.width * 0.05,
                              left: MediaQuery.of(context).size.width * 0.05,
                              bottom: 35),
                          child: _buildPassword(),
                        ),
                        _buildErrorText(),
                        _getStartedbutton(),
                        Container(
                          alignment: Alignment.center,
                          height: 60,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text('Already Have an Account?',
                                  style: TextStyle(
                                      fontSize:
                                          MediaQuery.of(context).size.width *
                                              0.03)),
                              SizedBox(
                                width: 3,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushReplacementNamed(
                                      context, '/login');
                                },
                                child: Text(
                                  'Sign In',
                                  style: TextStyle(
                                      fontSize:
                                          MediaQuery.of(context).size.width *
                                              0.04,
                                      color: Theme.of(context).primaryColor),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CustomDialog extends StatelessWidget {
  final String title, description, buttonText;

  CustomDialog({
    @required this.title,
    @required this.description,
    @required this.buttonText,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: 16,
              bottom: 16,
              left: 16,
              right: 16,
            ),
            decoration: new BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10.0,
                  offset: const Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min, // To make the card compact
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 16.0),
                Text(
                  description,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(height: 24.0),
                Align(
                  alignment: Alignment.bottomRight,
                  child: FlatButton(
                    onPressed: () async {
                      Navigator.of(context).pop(); // To close the dialog
                    },
                    child: Text(buttonText),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
